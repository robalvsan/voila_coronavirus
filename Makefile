default:
	@printf "$$HELP"

help:
	@printf "$$HELP"

heroku-deploy:
	git push heroku master

define HELP
Please execute "make <command>". Example: make help
Available commands
- make heroku-deploy
\tDeploys the master branch into heroku.

endef

export HELP
